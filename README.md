View this project on [CADLAB.io](https://cadlab.io/project/24378). 

# Kosmic Superspreader
[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]
[![hits](https://hits.deltapapa.io/github/sonosus/kosmicsuperspreader.svg)](https://hits.deltapapa.io)

## About

This is a Kosmo format 'spread' module.
It takes a CV input and a Spread input.
The Spread input is:
* added to the CV input and sent to the + output.
* subtracted from the CV input and sent to the - output.
The CV input is also buffered and sent to the Buffer output - no multiples needed.

The Spread pot is used as an offset for the Spread input if a jack is plugged in, if not the Spread jack must be normalled to ground.
The Spread pot swings between +- 12v.
![image](https://aws1.discourse-cdn.com/free1/uploads/lookmumnocomputer/original/2X/7/76f274680969e396d5c10117ded16afb1bb8b4b6.jpeg)
![image](https://aws1.discourse-cdn.com/free1/uploads/lookmumnocomputer/original/2X/2/2d627ca549847d63316f2b9fbd052e00bbfdd0bf.png)

Interactive BOM [here](https://htmlpreview.github.io/?https://github.com/Sonosus/KosmicSuperspreader/blob/main/Docs/KosmicSuperspreader.html).

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
